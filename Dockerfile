FROM docker.io/library/ubuntu:22.04 AS base

COPY adder.pl adder.sh /app/

WORKDIR /app



###############################################################################
FROM base AS test

RUN perl -c adder.pl                 \
    && test $(./adder.pl 1 1) -eq 2  \
    && test $(./adder.sh 1 1) -eq 2  \
    && touch /.tests-successful



###############################################################################
FROM base

COPY --from=test /.tests-successful /

CMD /app/adder.pl
