#!/usr/bin/perl
use strict;
use warnings;
use v5.10;

die("supply two ints") unless($#ARGV == 1
            && $ARGV[0] =~ /^\d+$/
	    && $ARGV[1] =~ /^\d+$/);

say $ARGV[0] + $ARGV[1];
