# Gitlab CI/CD - example for Basj and Perl scripts

An example project showing a `.gitlab-ci.yml` that builds a Docker
image and runs tests on the application packaged in it.

This project uses example applications in Bash and Perl
for testing.


## Configuration
To make this example work in your project, you need to set this variable in
Gitlab project -> Settings -> CI/CD -> Variables:

```
docker_images_jobtechdev_se_passwd: <ask Calamari about the password>
```
