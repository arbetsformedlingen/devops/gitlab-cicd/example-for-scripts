#!/bin/bash

if [ "$#" -ne 2 ] \
   || [[ "$1" =~ /^[0-9]*$/ ]] \
   || [[ "$1" =~ /^[0-9]*$/ ]]; then
    echo "supply two ints" >&2; exit 1
fi

echo $(( "$1" + "$2" ))
